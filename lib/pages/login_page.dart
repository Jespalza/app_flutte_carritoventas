import 'package:appcarritoventa/controller/login_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
      init: LoginController(),
      builder: (_) => Scaffold(
        body: Stack(children: <Widget>[
          _crearFondo(_),
          _loginForm(_),
        ]),
      ),
    );
  }
}

Widget _crearFondo(_) {
  final fondoRojo = Container(
    height: Get.height,
    width: double.infinity,
    decoration: BoxDecoration(
      gradient: LinearGradient(
        colors: <Color>[
          Color.fromRGBO(250, 100, 0, 1.0),
          Color.fromRGBO(250, 54, 12, 1.0)
        ],
      ),
    ),
  );
  final circulo = Container(
    width: 100.0,
    height: 100.0,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: Color.fromRGBO(255, 255, 255, 0.3)),
  );
  return Stack(
    children: <Widget>[
      fondoRojo,
      Positioned(
        child: circulo,
      ),
      Positioned(
        child: circulo,
        top: -40,
        left: -30,
      ),
      Positioned(
        child: circulo,
        bottom: -40,
        right: 20,
      ),
      Positioned(
        child: circulo,
        top: -40,
        right: -30,
      ),
      Container(
        padding: EdgeInsets.only(top: 80.0),
        child: Column(
          children: <Widget>[
            Image(
              image: AssetImage('assets/logo.png'),
              width: 180,
            ),
            SizedBox(
              height: 10,
              width: double.infinity,
            ),
          ],
        ),
      )
    ],
  );
}

Widget _loginForm(_) {
  return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        SafeArea(
          child: Container(
            height: 180.0,
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 30.0),
          width: Get.width * 0.85,
          padding: EdgeInsets.symmetric(vertical: 50.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black38,
                    blurRadius: 3.0,
                    offset: Offset(0.0, 5.0))
              ]),
          child: Column(
            children: <Widget>[
              Text(
                'Ingreso ',
                style: TextStyle(
                  fontSize: 30.0,
                  color: Color.fromRGBO(250, 54, 12, 1.0),
                  fontFamily: 'nasalization',
                ),
              ),
              SizedBox(
                height: 60.0,
              ),
              _crearEmail(_),
              SizedBox(
                height: 30.0,
              ),
              _crearPass(_),
              SizedBox(
                height: 60.0,
              ),
              _crearBoton(_),
              SizedBox(
                height: 60.0,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 100.0,
        )
      ],
    ),
  );
}

Widget _crearBoton(_) {
  return RaisedButton(
    child: Container(
      padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
      child: Text(
        'Ingresar',
        style: TextStyle(
          fontFamily: 'nasalization',
        ),
      ),
    ),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
    elevation: 1.0,
    color: Color.fromRGBO(250, 54, 12, 1.0),
    textColor: Colors.white,
    onPressed: () {},
    // onPressed: snapshot.hasData ? () => _login(bloc, context, api) : null,
  );
}

Widget _crearPass(_) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(
          Icons.lock_outline,
          color: Color.fromRGBO(250, 54, 12, 1.0),
        ),
        labelText: 'Contraseña',
        // counterText: _.,
        // errorText: snapshot.error,
      ),
      onChanged: (value) {
        _.Pass(value);
      },
    ),
  );
}

Widget _crearEmail(_) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(
            Icons.person_outline,
            color: Color.fromRGBO(250, 54, 12, 1.0),
          ),
          labelText: 'Usuario',
          // counterText: _.userLogin,
          // errorText: snapshot.error,
        ),
        onChanged: (value) {
          _.User(value);
        }),
  );
}

// _login(_) async {
//   final api = ApiRest();
//   if (_.userLogin.toString() != '' && _.passLogin.toString() != '') {
//     final bool ingreso =
//         await api.login(_.userLogin.toString(), _.passLogin.toString());
//     print('ingreso');
//     print(ingreso);
//     if (ingreso == true) {
//       print("entra a home");
//       Get.to(HomePage());
//       Navigator.pushReplacementNamed(context, 'home_carnet');
//     } else {
//       Get.defaultDialog(
//         title: 'Error Login!',
//         titleStyle: TextStyle(
//           fontSize: 25,
//           color: Colors.red,
//           fontFamily: 'nasalization',
//         ),
//         middleText:
//             'Usuario o contraseña incorrecta, corrija las credenciales he inténtelo de nuevo.',
//       );
//     }
//   } else {
//     Get.defaultDialog(
//       title: 'Error Login!',
//       titleStyle: TextStyle(
//         fontSize: 25,
//         color: Colors.red,
//         fontFamily: 'nasalization',
//       ),
//       middleText: 'Los datos son requeridos para el ingreso.',
//     );
//   }
// }
